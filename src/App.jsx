import { SignUp } from "./components/signUp";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { SignIn } from "./components/signIn";
import { Home } from "./components/Home";
import { Detail } from "./components/Detail";
import { Profile } from "./components/Profile";
import { Pdf } from "./components/Pdf";

function App() {
  return (
    <Router>
      <Switch>
        <Route component={Pdf} path="/pdf/detail" />
        <Route component={Profile} path="/profile" />
        <Route component={Detail} path="/detail" />
        <Route component={SignUp} path="/sign-up" />
        <Route component={SignIn} path="/sign-in" />
        <Route component={Home} path="/" />
      </Switch>
    </Router>
  );
}

export default App;
