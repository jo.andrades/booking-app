import { useRef, useState, useEffect } from "react";
import jwt_decode from "jwt-decode";
import * as axios from "axios";
import { handleAlert } from "../utils/alerts";
import { spinner } from "../utils/spinner";
import { AlertTypesEnum } from "../utils/enums/alert.types.enum";
import Avatar from "react-avatar";

export const Profile = (props) => {
  const [user, setUser] = useState({ roles: [] });
  const [userInfo, setUserInfo] = useState({});
  const [userBookings, setUserBookings] = useState({ docs: [] });
  const [userToken, setUserToken] = useState(null);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [alert, setAlert] = useState({
    type: "",
    key: "",
  });

  useEffect(() => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      const decodeUser = jwt_decode(token);
      setUserToken(token);
      setUser(decodeUser);
      getUserInfo(token, decodeUser._id);
      getUserBookings(token);
    } else {
      props.history.push("/sign-in");
    }
  }, []);

  const getUserInfo = async (token, userId) => {
    try {
      const {
        data: { data },
      } = await axios.get(
        `http://localhost:3001/account/users/specific/${userId}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      setUserInfo(data);
    } catch (error) {
      console.log(error);
    }
  };

  const getUserBookings = async (token) => {
    try {
      const {
        data: { data },
      } = await axios.get(
        `http://localhost:3009/business/bookings/user/all-bookings`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      setUserBookings(data);
    } catch (error) {
      console.log(error);
    }
  };
  console.log(userBookings);
  return (
    <div className="container-fluid">
      <nav
        className="navbar navbar-expand-lg"
        style={{
          backgroundColor: "#6C63FF",
          paddingLeft: "20px",
          paddingRight: "20px",
          borderBottomLeftRadius: "3px",
          borderBottomRightRadius: "3px",
        }}
      >
        <a className="navbar-brand" href="#" style={{ color: "white" }}>
          Turismo real
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link" href="/" style={{ color: "white" }}>
                Inicio
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/#" style={{ color: "white" }}>
                Sobre nosotros
              </a>
            </li>
          </ul>
          {userToken ? (
            <ul className="navbar-nav ms-auto">
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="/profile"
                  data-bs-target="#myModal"
                  data-bs-toggle="modal"
                  style={{ color: "white" }}
                  onClick={() => {
                    props.history.push("/profile");
                  }}
                >
                  Hola, {user.name}!
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="#"
                  data-bs-target="#myModal"
                  data-bs-toggle="modal"
                  style={{ color: "red" }}
                  onClick={() => {
                    localStorage.removeItem("accessToken");
                    props.history.push("/sign-in");
                  }}
                >
                  Salir
                  <i
                    className="bi bi-box-arrow-right"
                    style={{
                      fontSize: "1rem",
                      color: "red",
                      paddingLeft: "8px",
                    }}
                  />
                </a>
              </li>
            </ul>
          ) : (
            <ul className="navbar-nav ms-auto">
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="#"
                  data-bs-target="#myModal"
                  data-bs-toggle="modal"
                  style={{ color: "white" }}
                  onClick={() => props.history.push("/sign-in")}
                >
                  Ingresar
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="#"
                  data-bs-target="#myModal"
                  data-bs-toggle="modal"
                  style={{ color: "white" }}
                  onClick={() => props.history.push("/sign-up")}
                >
                  Registrate
                </a>
              </li>
            </ul>
          )}
        </div>
      </nav>
      <br />
      <div className="row">
        <div className="col-md-3">
          <div
            className="card"
            style={{
              width: "18rem",
            }}
          >
            <Avatar
              name={user.name}
              style={{
                display: "block",
                marginLeft: "auto",
                marginRight: "auto",
                width: "40%",
              }}
            />
            <hr />
            <div className="card-body">
              <div
                style={{
                  marginBottom: "15px",
                }}
              >
                <i
                  className="bi bi-person-circle"
                  style={{
                    color: "#6C63FF",
                    padding: "7px",
                  }}
                />
                <span>
                  {user.name} {user.surnames}
                </span>
              </div>
              <div
                style={{
                  marginBottom: "15px",
                }}
              >
                <i
                  className="bi bi-envelope-fill"
                  style={{
                    color: "#6C63FF",
                    padding: "7px",
                  }}
                />
                <span>{user.email}</span>
              </div>
              <div
                style={{
                  marginBottom: "15px",
                }}
              >
                <i
                  className="bi bi-qr-code"
                  style={{
                    color: "#6C63FF",
                    padding: "7px",
                  }}
                />
                <span>{userInfo.dni}</span>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-9">
          {userBookings.docs.length === 0 ? (
            <h1
              style={{
                textAlign: "center",
              }}
            >
              Aún no has realizado una reserva.{" "}
              <a
                href="/"
                style={{
                  textDecoration: "none",
                }}
              >
                Hazla aquí
              </a>
            </h1>
          ) : (
            <div
              style={{
                position: "relative",
                height: "250px",
                overflow: "auto",
                display: "block",
              }}
            >
              <table className="table table-striped table-hover">
                <thead>
                  <tr>
                    <th
                      scope="col"
                      style={{
                        color: "#6C63FF",
                      }}
                    >
                      Fecha reserva
                    </th>
                    <th
                      scope="col"
                      style={{
                        color: "#6C63FF",
                      }}
                    >
                      Total
                    </th>
                    <th
                      scope="col"
                      style={{
                        color: "#6C63FF",
                      }}
                    >
                      Acciones
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {userBookings.docs.map((book) => (
                    <tr key={book._id}>
                      <td>{book.startAt.substring(0, 10)}</td>
                      <td>
                        $
                        {book.totalCost
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                      </td>
                      <td>
                        {book.bookingStatus === "reserved" ? (
                          <>
                            <a
                              href="/pdf/detail"
                              style={{ textDecoration: "none" }}
                            >
                              Descargar boleta
                            </a>
                            &nbsp; o &nbsp;
                            <a href="#" style={{ textDecoration: "none" }}>
                              Editar check-in
                            </a>
                          </>
                        ) : (
                          <>
                            <a
                              href="/pdf/detail"
                              style={{ textDecoration: "none" }}
                            >
                              Descargar boleta
                            </a>
                          </>
                        )}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
