import { useRef, useState, useEffect } from "react";
import jwt_decode from "jwt-decode";
import * as axios from "axios";
import { handleAlert } from "../utils/alerts";
import { spinner } from "../utils/spinner";
import { AlertTypesEnum } from "../utils/enums/alert.types.enum";

export const Detail = (props) => {
  const apartmentId = useRef(localStorage.getItem("apartment"));

  const [user, setUser] = useState({ roles: [] });
  const [apartment, setApartment] = useState({ value: "" });
  const [userToken, setUserToken] = useState(null);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [alert, setAlert] = useState({
    type: "",
    key: "",
  });
  const [tac, setTac] = useState(false);
  const [isAvailable, setIsAvailable] = useState(false);
  const [booking, setBooking] = useState({
    apartment: "",
    client: "",
    startAt: "",
    endAt: "",
    totalCost: 0,
  });

  useEffect(() => {
    const token = localStorage.getItem("accessToken");

    if (token) {
      setUserToken(token);
      setUser(jwt_decode(token));
      getAllApartment(token);
    } else {
      props.history.push("/sign-in");
    }
  }, []);

  const getCurrentIsoStringDate = () => {
    const today = new Date();
    today.setUTCHours(0, 0, 0, 0);
    return today.toISOString();
  };

  const getDays = (start, end) => {
    return (
      (new Date(end).getTime() - new Date(start).getTime()) /
      (1000 * 60 * 60 * 24)
    );
  };

  const getDates = async () => {
    const today = getCurrentIsoStringDate();

    if (
      !document.getElementById("startAt").value ||
      !document.getElementById("endAt").value
    ) {
      setError(true);
      setAlert({ type: AlertTypesEnum.VOID_INPUT_DATE, key: null });
      return;
    }

    const startAt = new Date(
      document.getElementById("startAt").value
    ).toISOString();
    const endAt = new Date(
      document.getElementById("endAt").value
    ).toISOString();

    if (endAt < startAt || startAt < today) {
      setError(true);
      setAlert({ type: AlertTypesEnum.INVALID_RANGE_DATE, key: null });
      return;
    }
    setError(false);
    setAlert({ type: null, key: null });

    try {
      const {
        data: { data },
      } = await axios.post(`http://localhost:3009/business/bookings/aparment`, {
        apartmentId: apartment._id,
        startAt,
        endAt,
      });

      console.log(data);

      if (!data) {
        setError(true);
        setAlert({ type: AlertTypesEnum.IS_AVAILABLE_ERROR, key: null });
        setIsAvailable(false);
        return;
      }

      setBooking({
        apartment: apartment._id,
        client: user._id,
        startAt,
        endAt,
        totalCost: getDays(startAt, endAt) * apartment.value,
      });

      setIsAvailable(true);
      setError(false);
    } catch (error) {
      console.log(error);
    }
  };

  const doBook = async (book) => {
    try {
      setLoading(true);
      const {
        data: { data },
      } = await axios.post(`http://localhost:3009/business/bookings`, book, {
        headers: { Authorization: `Bearer ${userToken}` },
      });

      if (data) {
        await getPaymentlink(data._id, data.totalCost);
      }
    } catch (error) {
      console.log(error);
      setLoading(false);
      setError(true);
      setAlert({
        type: null,
        key: null,
      });
    }
  };

  const getPaymentlink = async (bookingId, total) => {
    try {
      const {
        data: { data },
      } = await axios.post(
        `http://localhost:3009/business/bookings/get-payment-link`,
        {
          booking: bookingId,
          total: Math.round(total * 0.0012) / 2,
        }
      );

      if (data) {
        window.location.href = data;
      }
    } catch (error) {
      console.log(error);
      setLoading(false);
      setError(true);
      setAlert({
        type: null,
        key: null,
      });
    }
  };

  const getAllApartment = async (token) => {
    try {
      const {
        data: { data },
      } = await axios.get(
        `http://localhost:3008/administration/apartment/${apartmentId.current}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      setApartment(data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="container-fluid">
      {loading ? (
        spinner()
      ) : (
        <>
          <nav
            className="navbar navbar-expand-lg"
            style={{
              backgroundColor: "#6C63FF",
              paddingLeft: "20px",
              paddingRight: "20px",
              borderBottomLeftRadius: "3px",
              borderBottomRightRadius: "3px",
            }}
          >
            <a className="navbar-brand" href="#" style={{ color: "white" }}>
              Turismo real
            </a>
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="/" style={{ color: "white" }}>
                    Inicio
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="/#" style={{ color: "white" }}>
                    Sobre nosotros
                  </a>
                </li>
              </ul>
              {userToken ? (
                <ul className="navbar-nav ms-auto">
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      href="/profile"
                      data-bs-target="#myModal"
                      data-bs-toggle="modal"
                      style={{ color: "white" }}
                      onClick={() => {
                        props.history.push("/profile");
                      }}
                    >
                      Hola, {user.name}!
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      href="#"
                      data-bs-target="#myModal"
                      data-bs-toggle="modal"
                      style={{ color: "red" }}
                      onClick={() => {
                        localStorage.removeItem("accessToken");
                        props.history.push("/sign-in");
                      }}
                    >
                      Salir
                      <i
                        class="bi bi-box-arrow-right"
                        style={{
                          fontSize: "1rem",
                          color: "red",
                          paddingLeft: "8px",
                        }}
                      />
                    </a>
                  </li>
                </ul>
              ) : (
                <ul className="navbar-nav ms-auto">
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      href="#"
                      data-bs-target="#myModal"
                      data-bs-toggle="modal"
                      style={{ color: "white" }}
                      onClick={() => props.history.push("/sign-in")}
                    >
                      Ingresar
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link"
                      href="#"
                      data-bs-target="#myModal"
                      data-bs-toggle="modal"
                      style={{ color: "white" }}
                      onClick={() => props.history.push("/sign-up")}
                    >
                      Registrate
                    </a>
                  </li>
                </ul>
              )}
            </div>
          </nav>
          <br />
          <div className="row">
            <div className="col-md-12">
              <span>Selecciona fecha de inicio: </span>
              <input type="date" id="startAt" />
              &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
              <span>Selecciona fecha de fin: </span>
              <input type="date" id="endAt" />
              &nbsp; &nbsp;
              <button
                className="btn btn-primary"
                onClick={() => getDates()}
                style={{
                  backgroundColor: "#6C63FF",
                  height: "85%",
                  fontSize: "95%",
                }}
              >
                Buscar
              </button>
            </div>
          </div>
          {error ? handleAlert(alert.type, alert.key) : null}
          <br />
          <div className="row">
            <div className="col-md-4">
              <img
                src={apartment.photo}
                style={{
                  borderRadius: "8%",
                  height: "325px",
                  width: "325px",
                }}
              />
            </div>
            <div className="col-md-8">
              <h2
                style={{
                  textAlign: "center",
                  color: "#1A0DE8",
                }}
              >
                Información del departamento
              </h2>
              <hr />
              <div className="row">
                <div className="col-md-6">
                  <ul>
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-map-fill"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <span>Región: {apartment.region}</span>
                    </div>
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-signpost-split-fill"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <span>Dirección: {apartment.address}</span>
                    </div>
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-building"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <span>Piso del dpto: {apartment.floor}</span>
                    </div>
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-door-open-fill"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <span>Número del dpto: {apartment.number}</span>
                    </div>
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-people-fill"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <span>
                        Capacidad máxima:{" "}
                        <span style={{ color: "#1A0DE8" }}>
                          {apartment.maxCapacity}
                        </span>{" "}
                        personas
                      </span>
                    </div>
                  </ul>
                </div>
                <div className="col-md-6">
                  <ul>
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-lamp-fill"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <span>Número de habitaciones: {apartment.roomsNum}</span>
                    </div>
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-life-preserver"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <span>Número de baños: {apartment.bathroomNum}</span>
                    </div>
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-columns-gap"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <span>
                        Metros cuadrados: {apartment.m2} m<sup>2</sup>{" "}
                      </span>
                    </div>
                    {apartment.terrace ? (
                      <div
                        style={{
                          marginBottom: "15px",
                        }}
                      >
                        <i
                          className="bi bi-check-circle-fill"
                          style={{
                            color: "#6C63FF",
                            padding: "7px",
                          }}
                        />
                        <span>Terraza</span>
                      </div>
                    ) : null}
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-calendar-range-fill"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <span>
                        Valor por dia: $
                        {apartment.value
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                      </span>
                    </div>
                    <div
                      style={{
                        marginBottom: "15px",
                      }}
                    >
                      <i
                        className="bi bi-arrow-down"
                        style={{
                          color: "#6C63FF",
                          padding: "7px",
                        }}
                      />
                      <a href="#" onClick={() => setTac(!tac)}>
                        Terminos y condiciones
                      </a>
                    </div>
                    {tac ? (
                      <div className="alert alert-info" role="alert">
                        <i
                          className="bi bi-info-circle-fill"
                          style={{ marginRight: "10px" }}
                        />
                        <span>{apartment.terms}</span>
                      </div>
                    ) : null}
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div
            className="row"
            style={{
              paddingTop: "15px",
            }}
          >
            <div className="col-md-3">
              <i
                className="bi bi-wifi"
                style={{
                  fontSize: "2rem",
                  color: "#6C63FF",
                  paddingRight: "15px",
                }}
              />
              <i
                className="bi bi-tv"
                style={{
                  fontSize: "2rem",
                  color: "#6C63FF",
                  paddingRight: "15px",
                }}
              />
              <i
                class="bi bi-controller"
                style={{
                  fontSize: "2rem",
                  color: "#6C63FF",
                  paddingRight: "15px",
                }}
              />
              <p>
                <h3 style={{ color: "#1A0DE8" }}>Dpto. {apartment.region}</h3>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
                Similique, rerum ea quam exercitationem alias error deserunt
                suscipit vitae cumque iste maiores? Alias sit laudantium
                molestiae rerum quibusdam illo voluptas. Inventore?
              </p>
            </div>
            <div className="col-md-9">
              {isAvailable ? (
                <div className="d-grid gap-2">
                  <a
                    className="btn btn-primary"
                    type="button"
                    href="#"
                    style={{
                      backgroundColor: "#6C63FF",
                    }}
                    onClick={() => doBook(booking)}
                  >
                    Reservar por{" $"}
                    {booking.totalCost
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
                  </a>
                </div>
              ) : null}
            </div>
          </div>
        </>
      )}
    </div>
  );
};
