import { useEffect, useState } from "react";
import jwt_decode from "jwt-decode";
import * as axios from "axios";
import { token as machineToken } from "../utils/machine/machineToken.json";

export const Home = (props) => {
  const [userToken, setUserToken] = useState(null);
  const [user, setUser] = useState({ roles: [] });
  const [apartments, setApartments] = useState({
    docs: [],
  });

  useEffect(() => {
    const token = localStorage.getItem("accessToken");
    if (token) {
      setUserToken(token);
      setUser(jwt_decode(token));
      getAllApartments(token, true);
    } else {
      console.log(machineToken);
      getAllApartments(machineToken, true);
    }
  }, []);

  const getAllApartments = async (token, status) => {
    try {
      const {
        data: { data },
      } = await axios.get(
        `http://localhost:3008/administration/apartment?isAvailable=${status}`,
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );

      setApartments(data);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="container-fluid">
      <nav
        className="navbar navbar-expand-lg"
        style={{
          backgroundColor: "#6C63FF",
          paddingLeft: "20px",
          paddingRight: "20px",
          borderBottomLeftRadius: "3px",
          borderBottomRightRadius: "3px",
        }}
      >
        <a className="navbar-brand" href="#" style={{ color: "white" }}>
          Turismo real
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <a className="nav-link" href="/#" style={{ color: "white" }}>
                Inicio
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/#" style={{ color: "white" }}>
                Sobre nosotros
              </a>
            </li>
          </ul>
          {userToken ? (
            <ul className="navbar-nav ms-auto">
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="/profile"
                  data-bs-target="#myModal"
                  data-bs-toggle="modal"
                  style={{ color: "white" }}
                  onClick={() => {
                    props.history.push("/profile");
                  }}
                >
                  Hola, {user.name}!
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="#"
                  data-bs-target="#myModal"
                  data-bs-toggle="modal"
                  style={{ color: "red" }}
                  onClick={() => {
                    localStorage.removeItem("accessToken");
                    props.history.push("/sign-in");
                  }}
                >
                  Salir
                  <i
                    class="bi bi-box-arrow-right"
                    style={{
                      fontSize: "1rem",
                      color: "red",
                      paddingLeft: "8px",
                    }}
                  />
                </a>
              </li>
            </ul>
          ) : (
            <ul className="navbar-nav ms-auto">
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="#"
                  data-bs-target="#myModal"
                  data-bs-toggle="modal"
                  style={{ color: "white" }}
                  onClick={() => props.history.push("/sign-in")}
                >
                  Ingresar
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="#"
                  data-bs-target="#myModal"
                  data-bs-toggle="modal"
                  style={{ color: "white" }}
                  onClick={() => props.history.push("/sign-up")}
                >
                  Registrate
                </a>
              </li>
            </ul>
          )}
        </div>
      </nav>
      <br />
      <div className="row">
        <div className="col-md-6">
          <div className="card mb-3" style={{ width: "100%" }}>
            <div className="row g-0">
              <div className="col-md-4">
                <img
                  src="https://portfolio-jofas.s3.sa-east-1.amazonaws.com/statics/ptoVarasStatic.jpeg"
                  className="img-fluid rounded-start"
                  style={{ width: "100%" }}
                  alt="..."
                />
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <h5 className="card-title">
                    Puerto Varas, Región de los lagos
                  </h5>
                  <p className="card-text">
                    Escápate de la rutina y déjate sorprender en Puerto Varas.
                    Vas a descubrir la fusión perfecta entre naturaleza y
                    confort. Encontrarás un entorno lleno de paz, para disfrutar
                    del aire puro y la naturaleza.
                  </p>
                  <p className="card-text">
                    <small className="text-muted">Turismo Real</small>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6">
          <div className="card mb-3" style={{ width: "100%" }}>
            <div className="row g-0">
              <div className="col-md-4">
                <img
                  src="https://portfolio-jofas.s3.sa-east-1.amazonaws.com/statics/metropolitana.jpeg"
                  className="img-fluid rounded-start"
                  style={{ width: "100%" }}
                  alt="..."
                />
              </div>
              <div className="col-md-8">
                <div className="card-body">
                  <h5 className="card-title">Santiago, Región Metropolitana</h5>
                  <p className="card-text">
                    Encuentra en Santiago de Chile una ciudad cautivante.
                    Recorre sus calles, conoce sus secretos y siente la cultura
                    local. Podrás conocer pueblos típicos cordilleranos, lugares
                    con vistas panorámicas maravillosas.
                  </p>
                  <p className="card-text">
                    <small className="text-muted">Turismo Real</small>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br />
      <div
        style={{
          backgroundColor: "#6C63FF",
          color: "white",
          textAlign: "center",
          width: "100%",
          height: "100%",
          borderRadius: "3px",
        }}
      >
        Conoce nuestros depatamentos
      </div>
      <br />
      <div className="row">
        {apartments ? (
          apartments.docs.map((a) => (
            <div className="col-md-4" style={{ paddingBottom: "10px" }}>
              <div
                className="card"
                style={{
                  width: "22rem",
                }}
              >
                <img src={a.photo} className="card-img-top" alt="..." />
                <div className="card-body">
                  <h5 className="card-title">
                    <a
                      href="/detail"
                      onClick={() => {
                        localStorage.removeItem("apartment");
                        localStorage.setItem("apartment", a._id);
                      }}
                      style={{ textDecoration: "none" }}
                    >
                      Dpto. {a.region}
                    </a>
                  </h5>
                  <div className="row">
                    <div className="col-md-6">
                      <ul>
                        <li key={Math.random().toString()}>
                          Piezas: {a.roomsNum}
                        </li>
                        <li key={Math.random().toString()}>
                          Baños: {a.bathroomNum}
                        </li>
                      </ul>
                    </div>
                    <div className="col-md-6">
                      <ul>
                        <li key={Math.random().toString()}>M2: {a.m2}</li>
                        <li key={Math.random().toString()}>
                          Personas: {a.maxCapacity}
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))
        ) : (
          <h1>No se encuentran departamentos disponibles</h1>
        )}
      </div>
    </div>
  );
};
