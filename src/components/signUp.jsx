import { useState } from "react";
import * as axios from "axios";
import { handleAlert } from "../utils/alerts";
import { spinner } from "../utils/spinner";
import { AlertTypesEnum } from "../utils/enums/alert.types.enum";

export const SignUp = (props) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [alert, setAlert] = useState({
    type: "",
    key: "",
  });
  const [isRut, setIsRut] = useState(true);

  const [form, setForm] = useState({
    name: "",
    surnames: "",
    dniType: "",
    dni: "",
    email: "",
    password: "",
    nacionality: "",
    gender: "",
    role: "client",
  });

  const handleInputChange = (event) => {
    if (event.target.name !== "inlineRadioOptions") {
      setForm({
        ...form,
        [event.target.name]: event.target.value,
      });
    } else {
      event.target.value === "rut" ? setIsRut(true) : setIsRut(false);
      setForm({
        ...form,
        dniType: event.target.value,
      });
    }
  };

  const saveUser = async (event) => {
    event.preventDefault();

    const _voidInputs = Object.entries(form).map(([key, value]) => {
      if (!value.trim()) {
        return key;
      }
    });

    const voidInputs = _voidInputs.filter((input) => input !== undefined);

    if (voidInputs.length === 0) {
      if (form.dniType === "rut" && !rutValidator(form.dni)) {
        setError(true);
        setAlert({ type: AlertTypesEnum.INVALID_DNI, key: "rut" });
      } else {
        setLoading(true);

        const { status } = await axios.post(
          "http://localhost:3001/account/users",
          form
        );

        if (status === 201) {
          setLoading(false);
          props.history.push("/sign-in");
        }
      }
    } else {
      setError(true);
      setAlert({ type: AlertTypesEnum.VOID_INPUT, key: voidInputs.shift() });
    }
  };
  const rutValidator = (fullRut) => {
    if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(fullRut)) return false;
    var tmp = fullRut.split("-");
    var digv = tmp[1];
    var rut = tmp[0];
    if (digv == "K") digv = "k";
    return vdValidator(rut) == digv;
  };

  const vdValidator = (T) => {
    var M = 0,
      S = 1;
    for (; T; T = Math.floor(T / 10)) S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
    return S ? S - 1 : "k";
  };

  return (
    <div className="container">
      {loading ? (
        spinner()
      ) : (
        <div className="container">
          <div className="row">
            <div
              className="col-md-4"
              style={{
                backgroundImage: `url("https://portfolio-jofas.s3.sa-east-1.amazonaws.com/statics/photo2.png")`,
                backgroundPosition: "center",
                backgroundSize: ["800px", "800px"],
              }}
            ></div>
            <div className="col-md-8">
              <div className="card-body">
                <h5 className="card-title mb-4">Bienvenido a Turismo Real!</h5>
                {error ? handleAlert(alert.type, alert.key) : null}
                <form onSubmit={saveUser}>
                  <div className="form-content">
                    <div className="row">
                      <div className="form-group mb-2">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Nombre *"
                          name="name"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="form-group mb-2">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Apellido *"
                          name="surnames"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="row mb-2">
                        <div className="col-md-3">Tipo de documento:</div>
                        <div className="col-md-2">
                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              type="radio"
                              name="inlineRadioOptions"
                              value="rut"
                              onChange={handleInputChange}
                            />
                            <label
                              className="form-check-label"
                              for="inlineRadio1"
                            >
                              Rut
                            </label>
                          </div>
                        </div>
                        <div className="col-md-2">
                          <div className="form-check form-check-inline">
                            <input
                              className="form-check-input"
                              type="radio"
                              name="inlineRadioOptions"
                              value="passport"
                              onChange={handleInputChange}
                            />
                            <label
                              className="form-check-label"
                              for="inlineRadio1"
                            >
                              Pasaporte
                            </label>
                          </div>
                        </div>
                      </div>

                      <div className="form-group mb-2">
                        <input
                          type="text"
                          className="form-control"
                          placeholder={
                            isRut ? "Rut: ej (9111222-0) *" : "Pasaporte *"
                          }
                          name="dni"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="form-group mb-2">
                        <input
                          type="email"
                          className="form-control"
                          placeholder="Email *"
                          name="email"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="form-group mb-2">
                        <input
                          type="password"
                          className="form-control"
                          placeholder="Contraseña *"
                          name="password"
                          onChange={handleInputChange}
                        />
                      </div>

                      <div className="form-group mb-2">
                        <select
                          className="form-control"
                          name="nacionality"
                          onChange={handleInputChange}
                        >
                          <option value="">Selecciona tu nacionalidad</option>
                          <option value="chilean">Chilena</option>
                          <option value="other">Otra</option>
                        </select>
                      </div>

                      <div className="form-group mb-2">
                        <select
                          className="form-control"
                          name="gender"
                          onChange={handleInputChange}
                        >
                          <option value="">Selecciona tu genero</option>
                          <option value="male">Masculino</option>
                          <option value="female">Femenino</option>
                          <option value="other">Otro</option>
                        </select>
                      </div>

                      <div class="form-group mb-2">
                        <input
                          class="form-check-input"
                          type="checkbox"
                          style={{ marginRight: 4 }}
                          value=""
                          id="flexCheckDefault"
                        />
                        <label class="form-check-label" for="flexCheckDefault">
                          Acepta los{" "}
                          <a href="https://www.google.com">
                            Terminos y condiciones
                          </a>
                          .
                        </label>
                      </div>

                      <div className="d-grid gap-2">
                        <button className="btn btn-primary" type="submit">
                          Registrar
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
