import React from "react";
import {
  Page,
  Text,
  View,
  Document,
  StyleSheet,
  PDFViewer,
  Image,
} from "@react-pdf/renderer";

const styles = StyleSheet.create({
  page: {
    flexDirection: "row",
    backgroundColor: "#E4E4E4",
    height: "100%",
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1,
  },
  text: {
    margin: 10,
  },
  img: {
    margin: 10,
    width: 100,
    height: 100,
  },
});
export const Pdf = (props) => {
  return (
    <PDFViewer width="100%" height="800">
      <Document>
        <Page style={styles.page}>
          <Image
            style={styles.img}
            src="https://portfolio-jofas.s3.sa-east-1.amazonaws.com/statics/logo-gold.png"
          />
          <View style={styles.section}>
            <Text style={styles.text}>Id de la reserva: </Text>
            <Text style={styles.text}>Rut cliente: </Text>
            <Text style={styles.text}>Monto total: </Text>
          </View>
          <View style={styles.section}>
            <Text style={styles.text}>Fecha de compra: </Text>
            <Text style={styles.text}>Id del pago: </Text>
            <Text style={styles.text}>Medio: PayPal</Text>
          </View>
        </Page>
      </Document>
    </PDFViewer>
  );
};
