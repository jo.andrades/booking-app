import { useState } from "react";
import * as axios from "axios";
import { handleAlert } from "../utils/alerts";
import { spinner } from "../utils/spinner";
import { AlertTypesEnum } from "../utils/enums/alert.types.enum";

export const SignIn = (props) => {
  const [login, setLogin] = useState({
    username: "",
    password: "",
  });
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [alert, setAlert] = useState({
    type: "",
    key: "",
  });

  const handleInputChange = (event) => {
    setLogin({
      ...login,
      [event.target.name]: event.target.value,
    });
  };

  const submitUser = async (event) => {
    event.preventDefault();

    const _voidInputs = Object.entries(login).map(([key, value]) => {
      if (!value.trim()) {
        return key;
      }
    });

    const voidInputs = _voidInputs.filter((input) => input !== undefined);

    if (voidInputs.length === 0) {
      setLoading(true);

      try {
        const {
          status,
          data: { data },
        } = await axios.post("http://localhost:3000/auth/login", login);
        setLoading(false);
        localStorage.setItem("accessToken", data);
        props.history.push("/");
      } catch (error) {
        setLoading(false);
        setError(true);
        setAlert({ type: AlertTypesEnum.USER_NOT_FOUND, key: "" });
      }
    } else {
      setError(true);
      setAlert({ type: AlertTypesEnum.VOID_INPUT, key: voidInputs.shift() });
    }
  };

  return (
    <div className="container">
      {loading ? (
        spinner()
      ) : (
        <div className="row">
          <div className="col-md-4" style={{ backgroundColor: "#6C63FF" }} />
          <div className="col-md-6">
            <div className="note mb-4">
              <h5>Acceso clientes Turismo Real!</h5>
              {error ? handleAlert(alert.type, alert.key) : null}
            </div>
            <form onSubmit={submitUser}>
              <div class="mb-3">
                <input
                  type="email"
                  className="form-control"
                  placeholder="Email *"
                  name="username"
                  onChange={handleInputChange}
                />
              </div>
              <div class="mb-3">
                <input
                  type="password"
                  className="form-control"
                  placeholder="Contraseña *"
                  name="password"
                  onChange={handleInputChange}
                />
              </div>
              <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                <span>
                  No tienes cuenta?{" "}
                  <a
                    href="/sign-up"
                    style={{
                      textDecoration: "none",
                    }}
                  >
                    registrate
                  </a>
                </span>
              </div>
              <button type="submit" class="btn btn-primary">
                Iniciar sesión
              </button>
            </form>
          </div>
        </div>
      )}
    </div>
  );
};
