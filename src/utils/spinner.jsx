export const spinner = () => {
  return (
    <div
      class="spinner-border text-danger position-absolute top-50 start-50"
      role="status"
    >
      <span class="visually-hidden">Cargando...</span>
    </div>
  );
};
