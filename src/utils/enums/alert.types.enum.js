export const AlertTypesEnum = {
  VOID_INPUT: "void_input",
  USER_NOT_FOUND: "user_not_found",
  INVALID_DNI: "invalid_dni",
  INVALID_RANGE_DATE: "invalid_range_date",
  VOID_INPUT_DATE: "void_input_date",
  IS_AVAILABLE_ERROR: "is_available",
};
